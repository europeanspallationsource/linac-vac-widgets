PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvAccelerating   = 0;
var pvAtNominalSpeed = 0;
var pvStopped        = 0;
var pvError          = 0;

var pvStatus         = pvs[0];

var sum     = 0;
var isValid = 0;
var status  = "N/A";

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvAccelerating   = 1 * PVUtil.getInt(pvs[1]);
	pvAtNominalSpeed = 2 * PVUtil.getInt(pvs[2]);
	pvStopped        = 4 * PVUtil.getInt(pvs[3]);
	pvError          = 8 * PVUtil.getInt(pvs[4]);

	sum              = pvAccelerating | pvAtNominalSpeed | pvStopped | pvError;
	isValid          = (sum & (sum - 1)) == 0 ? 1 : 0;

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	if (pvError) {
		Logger.info(pvSymbol + ": ERROR");
		status = "ERROR";
	} else if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvStopped) {
		Logger.info(pvSymbol + ": STOPPED");
		status = "STOPPED";
	} else if (pvAtNominalSpeed) {
		Logger.info(pvSymbol + ": NOMINAL-SPEED");
		status = "At NOMINAL-SPEED";
	} else if (pvAccelerating) {
		Logger.info(pvSymbol + ": ACCELERATING");
		status = "ACCELERATING";
	} else
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvStatus.write(status);
