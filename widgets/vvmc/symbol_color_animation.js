PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvMode   = -1;

var pvSymbol = pvs[0];

var colorID  = 0;

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvStat   = PVUtil.getString(pvs[1]);
	pvMode   = PVUtil.getInt(pvs[2]);

	log_pv(pvs[1]);
	log_pv(pvs[2]);

	if (pvStat == "ON") {
		switch (pvMode) {
			case 0:
				Logger.info(pvSymbol + ": Open");
				colorID = 1;
				break;
			case 1:
				Logger.info(pvSymbol + ": Closed");
				colorID = 2;
			case 2:
			case 3:
			case 4:
			case 5:
				Logger.info(pvSymbol + ": NO-COLOR-CODES-FOR-STATE");
				colorID = 0;
				break;
			default:
				Logger.severe(pvSymbol + ": Unknown mode:" + pvMode);
				break;
		}
	} else if (pvStat == "ERROR") {
		colorID = 3;
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
