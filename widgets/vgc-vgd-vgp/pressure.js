PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var tooltip  = "N/A";

function log_pv(pv) {
	try {
		Logger.info(pv + ": " + PVUtil.getString(pv));
	} catch (err) {
		Logger.severe(err);
	}
}

try {
	var pvStat   = PVUtil.getString(pvs[0]);
	var pvPrsStr = PVUtil.getString(pvs[2]);
	var pvUnit   = PVUtil.getString(pvs[3]);

	log_pv(pvs[0]);
	log_pv(pvs[1]);
	log_pv(pvs[2]);

	var pressure;
	try {
		var vtype = PVUtil.getVType(pvs[1]);

		FormatOption = org.csstudio.display.builder.model.properties.FormatOption;
		FormatOptionHandler = org.csstudio.display.builder.model.util.FormatOptionHandler;

		pressure = FormatOptionHandler.format(vtype, FormatOption.EXPONENTIAL, 3, true);
	} catch (err) {
	}

	if (pvStat == "ON") {
		if (!pressure)
			tooltip = pvPrsStr + " " + pvUnit;
		else
			tooltip = pressure;
	} else if (pvStat == "OVER-RANGE" || pvStat == "UNDER-RANGE") {
		if (!pressure)
			tooltip = PVUtil.getDouble(pvs[1]) + " " + pvUnit;
		else
			tooltip = pressure;
	} else
		tooltip = pvPrsStr;
} catch (err) {
	Logger.severe(err);
}

widget.setPropertyValue("tooltip", tooltip);
