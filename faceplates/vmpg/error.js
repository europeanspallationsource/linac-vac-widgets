PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var errorText = "";

Logger.info("Error code: " + PVUtil.getLong(pvs[0]));
switch (PVUtil.getLong(pvs[0])) {
	case  1:
		errorText = "Turbo Pump Error";
		break;
	case  2:
		errorText = "Primary Pump Error";
		break;
	case  3:
		errorText = "Gauge Controller";
		break;
	case  4:
		errorText = "Full-Range Error";
		break;
	case  5:
		errorText = "VVP Error";
		break;
	case  6:
		errorText = "VVH Error";
		break;
	case  7:
		errorText = "VVD Error";
		break;
	case  8:
		errorText = "VVT Error";
		break;
	case  9:
		errorText = "VVV Error";
		break;
	case 11:
		errorText = "VVL Error";
		break;
	case 12:
		errorText = "VVS Error";
		break;
	case 16:
		errorText = "VVD Position Error";
		break;
	case  0:
		errorText = "No Errors";
		break;
	default:
		errorText = "Unknown Error Code: " + PVUtil.getString(pvs[0]);
		break;
}
Logger.info("Error text:" + errorText);

try {
	pvs[1].setValue(errorText);
} catch (err) {
	Logger.warning(err);
}
